<?php

namespace Webuni\AdminBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;

/**
 * Description of DashboardController
 *
 * @Route("/admin")
 *
 * @author Petr Jaša
 * @package Webuni\AdminBundle\Controller
 */
class DashboardController extends Controller
{
    /**
     * @Route("/index")
     * @Template()
     */
    public function indexAction()
    {
        return array();
    }

    /**
     * @Route("/hello/{name}")
     * @Security("is_granted('ROLE_ADMIN')")
     * @Template()
     *
     * @param $name
     * @return array
     */
    public function helloAction($name)
    {
        return array('name' => $name);
    }
}

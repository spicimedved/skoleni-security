<?php

namespace Webuni\AdminBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Acl\Domain\ObjectIdentity;
use Symfony\Component\Security\Acl\Domain\UserSecurityIdentity;
use Symfony\Component\Security\Acl\Permission\MaskBuilder;
use Symfony\Component\Security\Core\SecurityContextInterface;
use Webuni\AdminBundle\Entity\Article;
use Webuni\AdminBundle\Entity\User;

/**
 * Description of UserController
 *
 * @Route("/article")
 *
 * @author Petr Jaša
 * @package Webuni\AdminBundle\Controller
 */
class ArticleController extends Controller
{
    /**
     * @Route("/create")
     * @Method({"GET", "POST"})
     * @Template()
     *
     * @param Request $request
     * @return array
     */
    public function createAction(Request $request)
    {
        $user = $this->get('security.context')->getToken()->getUser();
        if (!$user instanceof User) {
            $this->createAccessDeniedException();
        }

        $article = new Article();
        $article->setTitle('Title');
        $article->setContent('Text článku.');
        $article->setUser($user);

        $em = $this->getDoctrine()->getManager();
        $em->persist($article);
        $em->flush();

        return $this->redirect("/article/list");
    }

    /**
     * @Route("/add")
     * @Method({"GET"})
     * @Template()
     *
     * @throws \Exception
     * @throws \Symfony\Component\Security\Acl\Exception\AclAlreadyExistsException
     */
    public function addAction()
    {
        $user = $this->get('security.context')->getToken()->getUser();
        if (!$user instanceof User) {
            $this->createAccessDeniedException();
        }

        $article = new Article();
        $article->setTitle('Title ACL article');
        $article->setContent('Text ACL článku.');
        $article->setUser($user);

        $em = $this->getDoctrine()->getManager();
        $em->persist($article);
        $em->flush();

        // creating the ACL
        $aclProvider = $this->get('security.acl.provider');
        $objectIdentity = ObjectIdentity::fromDomainObject($article);
        $acl = $aclProvider->createAcl($objectIdentity);

        // retrieving the security identity of the currently logged-in user
        $securityContext = $this->get('security.context');
        $user = $securityContext->getToken()->getUser();
        $securityIdentity = UserSecurityIdentity::fromAccount($user);

        // grant owner access
        $acl->insertObjectAce($securityIdentity, MaskBuilder::MASK_OWNER);
        $aclProvider->updateAcl($acl);

        $builder = new MaskBuilder();
        $builder
            ->add('view')
//            ->add('edit')
        ;
        $mask = $builder->get();
        $identity = new UserSecurityIdentity('pavel', 'Webuni\AdminBundle\Entity\User');
        $acl->insertObjectAce($identity, $mask);
        $aclProvider->updateAcl($acl);

        return $this->redirect("/article/list");
    }

    /**
     * @Route("/list")
     * @Method({"GET", "POST"})
     *
     * @Template()
     */
    public function listAction()
    {
        $articles = $this->getDoctrine()
            ->getRepository('WebuniAdminBundle:Article')
            ->findAll();

        return array(
            'articles' => $articles
        );
    }

    /**
     * @Route(
     *      "/show/{id}.{_format}",
     *      defaults={"_format"="html"},
     *      requirements={"id"="\d+", "_format"="html|xml|json"}
     * )
     * @Method({"GET", "POST"})
     * @Template()
     *
     * @param $id
     * @return array
     */
    public function showAction($id)
    {
        $article = $this->getDoctrine()
            ->getRepository('WebuniAdminBundle:Article')
            ->find($id);

        if (!$article) {
            throw $this->createNotFoundException(
                'No article found for id '.$id
            );
        }

        if (!$this->get('security.context')->isGranted('SHOW_OWN_ARTICLE', $article)) {
            throw $this->createAccessDeniedException();
        }

        return array(
            "article" => $article
        );
    }

    /**
     * @Route(
     *      "/show-acl/{id}.{_format}",
     *      defaults={"_format"="html"},
     *      requirements={"id"="\d+", "_format"="html|xml|json"}
     * )
     * @Method({"GET", "POST"})
     * @Template()
     *
     * @param $id
     * @return array
     */
    public function showAclAction($id)
    {
        $article = $this->getDoctrine()
            ->getRepository('WebuniAdminBundle:Article')
            ->find($id);

        if (!$article) {
            throw $this->createNotFoundException(
                'No article found for id '.$id
            );
        }

        if (false === $this->get('security.context')->isGranted('DELETE', $article)) {
            throw $this->createAccessDeniedException();
        }

        return array(
            "article" => $article
        );
    }
}

<?php

namespace Webuni\AdminBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\SecurityContextInterface;
use Webuni\AdminBundle\Entity\User;

/**
 * Description of UserController
 *
 * @Route("/user")
 *
 * @author Petr Jaša
 * @package Webuni\AdminBundle\Controller
 */
class UserController extends Controller
{
    /**
     * @Route("/registration")
     * @Template()
     *
     * @param Request $request
     * @return array
     */
    public function registrationAction(Request $request)
    {
        // vytvoříme registrační formulář
        $user = new User();
        $form = $this->createForm('webuni_user', $user, array(
            'method' => 'POST'
        ));

        // zkontrolujeme obdržený request
        // pokud obsahuje data formuláře a je odeslaný nastavenou http metodou
        // tak data vezmeme a naplníme je do objektu $user
        $form->handleRequest($request);

        // validujeme $user objekt a pokud je validní, tak provádíme další operace
        if ($form->isValid()) {
            // encode password
            $encoderFactory = $this->get('security.encoder_factory');
            $encoder = $encoderFactory->getEncoder($user);
            $user->setPassword($encoder->encodePassword($user->getPassword(), $user->getSalt()));

            // persist user
            $em = $this->getDoctrine()->getManager();
            $em->persist($user);
            $em->flush();

            // redirection
            return $this->redirect($this->generateUrl('webuni_admin_user_successregistration'));
        }

        return array(
            'form' => $form->createView()
        );
    }

    /**
     * @Route("/registration-success")
     * @Template("WebuniAdminBundle:User:registered.html.twig");
     *
     * @param Request $request
     * @return array
     */
    public function successRegistrationAction(Request $request)
    {
        return array(
            'message' => 'Registration successful'
        );
    }

    /**
     * @Route("/create")
     * @Template()
     *
     * @param Request $request
     * @return array
     */
    public function createAction(Request $request)
    {
        $name = $request->query->get("name", 'user_' . rand(1, 9999));
        $pass = $request->query->get("pass", 'user');
        $user = new User();

        $encoderFactory = $this->get('security.encoder_factory');
        $encoder = $encoderFactory->getEncoder($user);

        $user->setUsername($name);
        $user->setFirstname($name);
        $user->setPassword($encoder->encodePassword($pass, $user->getSalt()));
        $user->setRoles(array('ROLE_ADMIN'));

        $em = $this->getDoctrine()->getManager();
        $em->persist($user);
        $em->flush();

        return $this->redirect("/login");
    }
}

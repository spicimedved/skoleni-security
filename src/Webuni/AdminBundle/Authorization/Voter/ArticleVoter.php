<?php
namespace Webuni\AdminBundle\Authorization\Voter;

use Doctrine\Bundle\DoctrineBundle\Registry;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Authorization\Voter\VoterInterface;
use Webuni\AdminBundle\Entity\Article;
use Webuni\AdminBundle\Entity\User;

/**
 * Description of ArticleVoter
 *
 * @author Petr Jaša
 * @package Webuni\AdminBundle\Authorization\Voter
 */
class ArticleVoter implements VoterInterface
{
    /** @var \Doctrine\Bundle\DoctrineBundle\Registry */
    protected $doctrine;

    /** @var \Symfony\Component\HttpFoundation\RequestStack */
    protected $requestStack;

    /** @var array */
    protected $blacklistedIp;

    /**
     * @var array
     */
    protected $allowedAttributes = array(
        'SHOW_ARTICLE', 'SHOW_OWN_ARTICLE', 'EDIT_ARTICLE', 'EDIT_OWN_ARTICLE'
    );

    /**
     * @param RequestStack $requestStack
     * @param Registry $doctrine
     * @param array $blacklistedIp
     */
    public function __construct(RequestStack $requestStack, Registry $doctrine, array $blacklistedIp = array())
    {
        $this->doctrine = $doctrine;
        $this->requestStack  = $requestStack;
        $this->blacklistedIp = $blacklistedIp;
    }

    /**
     * @param string $attribute
     * @return bool
     */
    public function supportsAttribute($attribute)
    {
        return (in_array($attribute, $this->allowedAttributes));
    }

    /**
     * @param string $class
     * @return bool
     */
    public function supportsClass($class)
    {
        // your voter supports all type of token classes, so return true
        return true;
    }

    public function vote(TokenInterface $token, $object, array $attributes)
    {
        /** @var User $user */
        /** @var Article $article */
        $user = $token->getUser();
        $article = $object;

        // check each attribute one by one
        foreach ($attributes as $attribute) {
            if (!$this->supportsAttribute($attribute) || !$this->supportsClass($object)) {
                continue;
            }

            if ('SHOW_ARTICLE' === $attribute){
                return VoterInterface::ACCESS_GRANTED;
            }

            if ('SHOW_OWN_ARTICLE' === $attribute){
//                if ($user->hasRole('ROLE_ADMIN')){
//                    return VoterInterface::ACCESS_GRANTED;
//                }

                $article = $this->doctrine->getRepository("WebuniAdminBundle:Article")->findOneBy(array(
                    "id" => $article->getId(),
                    "user" => $user
                ));

                if ($article instanceof Article) {
                    return VoterInterface::ACCESS_GRANTED;
                }

            }
        }

        return VoterInterface::ACCESS_DENIED;
    }
}
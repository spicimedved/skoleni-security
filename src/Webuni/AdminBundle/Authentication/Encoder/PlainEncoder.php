<?php
namespace Webuni\AdminBundle\Authentication\Encoder;

use Symfony\Component\Security\Core\Encoder\PasswordEncoderInterface;

class PlainEncoder implements PasswordEncoderInterface
{
    public function encodePassword( $raw, $salt )
    {
        return $raw;
    }

    public function isPasswordValid( $encoded, $raw, $salt )
    {
        return $encoded === $this->encodePassword( $raw, $salt );
    }
}
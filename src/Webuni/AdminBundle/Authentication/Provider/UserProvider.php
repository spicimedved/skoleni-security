<?php
namespace Webuni\AdminBundle\Authentication\Provider;

use Doctrine\Bundle\DoctrineBundle\Registry;
use Symfony\Component\Security\Core\Encoder\EncoderFactory;
use Symfony\Component\Security\Core\Exception\UnsupportedUserException;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Security\Core\User\UserProviderInterface;
use Webuni\AdminBundle\Entity\SoapUser;

/**
 * Description of UserProvider
 *
 * @author Petr Jaša
 */
class UserProvider implements UserProviderInterface
{
    /** @var \Symfony\Component\Security\Core\Encoder\EncoderFactory  */
    protected $encoderFactory;

    public function __construct(EncoderFactory $encoderFactory, Registry $doctrine)
    {
        $this->encoderFactory = $encoderFactory;
    }

    public function loadUserByUsername($username)
    {
        $user = new SoapUser();
        $salt = 'martin-salt';

        $encoder = $this->encoderFactory->getEncoder($user);

        $user->setUsername('martin');
        $user->setPassword($encoder->encodePassword('martin', $salt));
        $user->setSalt($salt);
        $user->setFirstname('Martin');
        $user->setRoles(array('ROLE_EDITOR'));

        return $user;

//        throw new UsernameNotFoundException(
//            sprintf('Username "%s" does not exist.', $username)
//        );
    }

    public function refreshUser(UserInterface $user)
    {
        if (!$user instanceof SoapUser) {
            throw new UnsupportedUserException(
                sprintf('Instances of "%s" are not supported.', get_class($user))
            );
        }

        return $this->loadUserByUsername($user->getUsername());
    }

    public function supportsClass($class)
    {
        return $class === 'Webuni\AdminBundle\Entity\User';
    }
}